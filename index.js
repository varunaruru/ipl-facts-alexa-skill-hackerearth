/* eslint-disable  func-names */
/* eslint quote-props: ["error", "consistent"]*/
/**
 * This sample demonstrates a simple skill built with the Amazon Alexa Skills
 * nodejs skill development kit.
 * This sample supports multiple lauguages. (en-US, en-GB, de-DE).
 * The Intent Schema, Custom Slots and Sample Utterances for this skill, as well
 * as testing instructions are located at https://github.com/alexa/skill-sample-nodejs-fact
 **/

'use strict';
const Alexa = require('alexa-sdk');

//=========================================================================================================================================
//TODO: The items below this comment need your attention.
//=========================================================================================================================================

//Replace with your app ID (OPTIONAL).  You can find this value at the top of your skill's page on http://developer.amazon.com.
//Make sure to enclose your value in quotes, like this: const APP_ID = 'amzn1.ask.skill.bb4045e6-b3e8-4133-b650-72923c5980f1';
const APP_ID = undefined;

const SKILL_NAME = 'IPL facts and records';
const GET_FACT_MESSAGE = "Here's your fact: ";
const HELP_MESSAGE = 'You can say tell me a space fact, or, you can say exit... What can I help you with?';
const HELP_REPROMPT = 'What can I help you with?';
const STOP_MESSAGE = 'Goodbye!';

//=========================================================================================================================================
//TODO: Replace this data with your own.  You can find translations of this data at http://github.com/alexa/skill-sample-node-js-fact/data
//=========================================================================================================================================
const data = [
   "Virat kohli is the highest run scorer with 4649 runs",
"Chris gayle holds the record for hitting highest number of sixes",
"Highest individual score in a T20 match is 175 and it is scored by chris gayle against pune",
"Chris gayle scored 100 sixes more than rohit sharma who played 60 matches more than him",
" Opening Games of IPL always featured Mumbai Indians or Kolkata Knight Riders or both",
"Mahendra Singh Dhoni is the only player to Captain Chennai Super Kings in all the editions of the tournament",
"Suresh Raina has never missed a Match for his Franchise in IPL",
"Out of 13 teams which took part in IPL, 5 different teams have managed to win the Trophy at least once",
"Rajasthan Royals has won the inaugural edition in 2008",
"Deccan Chargers Hyderabad won the tournament in 2009 which was held in South Africa",
"Chennai Super Kings has won on two different occasions 2010 & 2011",
"Mumbai Indians also won the tournament two times in 2013 & 2015",
"Kolkata Knight Riders has also won the IPL trophy in two editions in 2012 & 2014",
"Sachin Tendulkar was the first Indian player to win Orange cap",
"Chris Gayle remained UNSOLD in 2011’s IPL Player Auction. Later he was taken by RCB as a replacement to injured Dirk Nannes",
"Chris Gayle and David warner won orange cap twice",
"Sohail Tanvir is the only pakistan player to win an IPL purple cap award",
"Sachin Tendulkar and Virat Kohli are the only Indian players to win MOst Valuable Player award in IPL",
"Pollard and Cutting are the only foreign players to win the man of the match award in IPL Final match",
"Royal challengers Bangalore score lowest IPL total 49 runs in 2017 against Kolkata",
"Virat Kohli hold the record for most number of runs in a single edition – 973 Runs",
"CSK have been a part of 6 Finals, most by any Team in IPL",
"Rohit sharma lifted IPL trophy 4 times most by any player",
"Of all the Teams who have 4 or more years experience in IPL, Delhi Daredevils is the only Team to NOT reach the IPL Final at least once",
"Parthiv Patel has played for 6 different Teams in 10 years of IPL",
"Gautam Gambhir holds the record for most number of 50’s and most number of ducks",
"Total number of Centuries recorded in IPL are 44",
"Chris Gayle has scored 6 Centuries in IPL, most by any player",
"Only 2 uncapped Players has scored a Century in IPL, Manish Pandey in 2009 & Paul Valthaty in 2011",
"Lasith Malinga has taken most number of wickets",
"Sunil Naraine has the best Economy rate",
"Amit Mishra has 3 hat tricks in IPL, most by any player in IPL",
"DJ Bravo has taken the most number of wickets (32) in a single edition. He also has won 2 Purple Caps, most by any player",
"Brad Hogg is the Oldest player to play in IPL",
"Piyush Chawla has bowled more than 400 overs in his IPL career. He is yet to bowl a no-ball!",
"Manish Pandey and Robin Uthappa both have played for 4 different IPL Teams but they both have been in the same team for 8 seasons",
"In 2013 Dale Steyn bowled 212 dot balls, highest by any bowler. In total he bowled 407 balls in that season, more than 50% dot balls",
"Delhi Daredevils is only team that has never played an IPL final",
"Praveen kumar bowled 14 maiden overs - most by any",
"Total number of Hat-Tricks recorded in IPL are 17",
"Dinesh karthick has record for dismissing most numbers of players as wicket keeper",
"Suresh Raina caught more than 90 players in IPL!!",
"Mumbai Indians won most matches in IPL",
];

//=========================================================================================================================================
//Editing anything below this line might break your skill.
//=========================================================================================================================================

const handlers = {
    'LaunchRequest': function () {
        this.emit('GetNewFactIntent');
    },
    'GetNewFactIntent': function () {
        const factArr = data;
        const factIndex = Math.floor(Math.random() * factArr.length);
        const randomFact = factArr[factIndex];
        const speechOutput = GET_FACT_MESSAGE + randomFact;

        this.response.cardRenderer(SKILL_NAME, randomFact);
        this.response.speak(speechOutput);
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function () {
        const speechOutput = HELP_MESSAGE;
        const reprompt = HELP_REPROMPT;

        this.response.speak(speechOutput).listen(reprompt);
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(STOP_MESSAGE);
        this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function () {
        this.response.speak(STOP_MESSAGE);
        this.emit(':responseReady');
    },
};

exports.handler = function (event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    alexa.APP_ID = APP_ID;
    alexa.registerHandlers(handlers);
    alexa.execute();
};
